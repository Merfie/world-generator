﻿using System;

namespace WorldGenerator.Graph
{
    public class CoordinateNode
    {
        public int P { get; set; }
        public int Q { get; set; }
        public int R { get; set; }
        public int RawHeight { get; set; }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + P;
            result = 31 * result + Q;
            result = 31 * result + R;
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            CoordinateNode coord = (CoordinateNode) obj;
            return P == coord.P && Q == coord.Q && R == coord.R;
        }
    }
}