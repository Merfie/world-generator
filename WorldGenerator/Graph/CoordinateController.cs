﻿using System.Collections.Generic;
using System.Linq;
using Neo4jClient;

namespace WorldGenerator.Graph
{
    public class CoordinateController
    {
        private IGraphClient _connection;
        public CoordinateController(IGraphClient Connection)
        {
            _connection = Connection;
        }

        public CoordinateNode CreateCoordinate(int p, int q, int r)
        {
            return _connection.Cypher
                .Merge("(c:Coordinate {P:{P}, Q:{Q}, R:{R}})")
                .WithParams(new {P = p, Q = q, R = r})
                .Return(c => c.As<CoordinateNode>())
                .Results.First();
        }

        public bool CoordinateExists(int p, int q, int r)
        {
            // Things I learned the hard way: All of your code variable names
            // Must equal the variable name declared in the Match() block
            List<CoordinateNode> result = _connection.Cypher
                .Match("(c:Coordinate)")
                .Where((CoordinateNode c) => c.P == p)
                .AndWhere((CoordinateNode c) => c.Q == q)
                .AndWhere((CoordinateNode c) => c.R == r)
                .Return(c => c.As<CoordinateNode>())
                .Results.ToList();

            // There should always be exactly 1 of a coordinate if it exists
            return result.Count >= 1;
        }

        public List<CoordinateNode> GetCoordinates()
        {
            return _connection.Cypher
                .Match("(c:Coordinate)")
                .Return(c => c.As<CoordinateNode>())
                .Results.ToList();

        }

        public CoordinateNode GetCoordinate(int p, int q, int r)
        {
            return _connection.Cypher
                .Match("(c:Coordinate)")
                .Where((CoordinateNode c) => c.P == p)
                .AndWhere((CoordinateNode c) => c.Q == q)
                .AndWhere((CoordinateNode c) => c.R == r)
                .Return(c => c.As<CoordinateNode>())
                .Results.First();
        }

        public void UpdateHeight(CoordinateNode target, int height)
        {
            _connection.Cypher
                .Match("(source:Coordinate)")
                .OptionalMatch("(source)-[:IS]-(result:Coordinate)")
                .Where((CoordinateNode source) => source.P == target.P)
                .AndWhere((CoordinateNode source) => source.Q == target.Q)
                .AndWhere((CoordinateNode source) => source.R == target.R)
                .Set("source.RawHeight = {height}")
                .Set("result.RawHeight = {height}")
                .WithParam("height", height)
                .ExecuteWithoutResults();
        }
    }
}