﻿using Neo4jClient.Transactions;

namespace WorldGenerator.Graph
{
    public class TransactionController
    {
        private ITransactionalGraphClient _graphClient;

        public TransactionController(ITransactionalGraphClient client)
        {
            _graphClient = client;
        }

        public ITransaction BeginTransaction()
        {
            return _graphClient.BeginTransaction();
        }

        public void CommitTransaction(ITransaction transaction)
        {
            transaction.Commit();
        }

        public void EndTransaction()
        {
            _graphClient.EndTransaction();
        }
    }
}