﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator
{
    public class TriangleMap
    {
        #region Public Variable
        public int size;
        #endregion
        #region Private Variable
        private int?[][] heightArray;
        int triangleId;
        #endregion

        public TriangleMap(int triangleId, int size = 512)
        {
            this.size = size;
            heightArray = new int?[size + 1][];
            for(int i = 0; i <= size; i++)
            {
                heightArray[i] = new int?[i + 1];
                for(int j = 0; j < i + 1; j ++)
                {
                    heightArray[i][j] = null;
                }
            }
            this.triangleId = triangleId;
        }

        public int? GetHeight(int x, int y)
        {
            if (x < 0 || y < 0 || y > size || x >= heightArray[y].Length)
                return null;
            return heightArray[y][x];
        }
        public void SetHeight(int x, int y, int? height)
        {
            heightArray[y][x] = height;
        }
    }
}
