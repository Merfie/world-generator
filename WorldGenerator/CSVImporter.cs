﻿using System;
using System.IO;
using Neo4jClient;
using Neo4jClient.Transactions;

namespace WorldGenerator
{
    public class CSVImporter
    {
        private IGraphClient _graphClient;

        public CSVImporter(IGraphClient graphClient)
        {
            _graphClient = graphClient;
        }

        public void ImportNodes(string csvFilePath)
        {
            FileInfo fileInfo = new FileInfo(csvFilePath);
            _graphClient.Cypher
                .LoadCsv(new Uri("file:///" + csvFilePath), "node", withHeaders: true, periodicCommit:10000)
                .Create("(:Coordinate {P:toInt(node.P), Q:toInt(node.Q), R:toInt(node.R)})")
                .ExecuteWithoutResults();
        }
    }

}