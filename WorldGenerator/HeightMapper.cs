﻿using System;
using System.Collections.Generic;

namespace WorldGenerator
{
    public class HeightMapper
    {
        #region Public Variable
        int? maxHeight = int.MinValue;
        int? minHeight = int.MaxValue;

        int seaLevel;
        #endregion
        #region Private Variable
        TriangleMap triangle;
        SquareMap squareMap;
        int fullSize;
        int roughness = 8;
        Random rng;
        int seed;
        #endregion

        public HeightMapper(int seedValue=-1)
        {
            this.seed = seedValue;
            if (seedValue >= 0)
            {
                rng = new Random(seedValue);
            }
            else
            {
                rng = new Random();
            }
        }
        public void BuildTriangle(TriangleMap triangle)
        {
            this.triangle = triangle;
            fullSize = triangle.size;
            SetTriangleCorners();
            TriangleEdge(fullSize);
        }
        private void SetTriangleCorners()
        {
            if (triangle.GetHeight(0, 0) == null)
            {
                triangle.SetHeight(0, 0, rng.Next(fullSize * -2, fullSize * 2));
            }
            if (triangle.GetHeight(0, fullSize) == null)
            {
                triangle.SetHeight(0, fullSize, rng.Next(fullSize * -2, fullSize * 2));
            }
            if (triangle.GetHeight(fullSize, fullSize) == null)
            {
                triangle.SetHeight(fullSize, fullSize, rng.Next(fullSize * -2, fullSize * 2));
            }
        }
        private void TriangleEdge(int size)
        {
            /*
            This is a modified Diamond-square algorithm https://en.wikipedia.org/wiki/Diamond-square_algorithm
            1.The alogirthm works on triangles of size 2 ^ n + 1 and it assumes the corners of the triangles are set to random values
            2.The algorith sets the center and the midpoints of the edges to an average plus a random offset
            3.This creates 4 smaller triangle (think triforce)
            4.The algorithm then runs on the 4 smaller triangles
            5.Once the algorithm has run across all triangles of side length 3 the algorithm terminates since all values in the tringle have been set
            : param size: the 0 indexed length of a triangle side (length of a side - 1)
            */
            int half = size / 2;
            int scale = roughness * size;
            int y;
            int x;
            String orientation;
            if (half >= 1)
            {
                y = half;
                while(y < fullSize)
                {
                    x = half / 2;
                    orientation = "UP";
                    while(x < y)
                    {
                        if (triangle.GetHeight(x, y) == null)
                        {
                            SetTriangleCenter(x, y, half, (int)(rng.NextDouble() * scale * 2 - scale), orientation);
                        }
                        if (orientation.Equals("UP"))
                        {
                            orientation = "DOWN";
                        }
                        else
                        {
                            orientation = "UP";
                        }
                        x += half;
                    }
                    y += size;
                }
                y = half;
                String face = "EDGE";
                while(y <= fullSize)
                {
                    x = 0;
                    orientation = "LEFT";
                    while(x <= y)
                    {
                        if (triangle.GetHeight(x, y) == null)
                        {
                            SetTriangleEdge(x, y, half, (int)(rng.NextDouble() * scale * 2 - scale), orientation, face);
                        }
                        if(orientation.Equals("LEFT"))
                        {
                            orientation = "Right";
                        }
                        else
                        {
                            orientation = "LEFT";
                        }
                        x += half;
                    }
                    if(face.Equals("EDGE"))
                    {
                        face = "BOTTOM";
                    }
                    else
                    {
                        face = "EDGE";
                    }
                    y += half;
                }
                TriangleEdge(half);
            }
        }
        private void SetTriangleCenter(int x, int y, int size, int offset, String orientation)
        {
            /*
             sets the center of the triangle to a the average of the 3 corners of the triangle
            :param x: the x coordinate of the edge midpoint
            :param y: the y coordinate of the edge midpoint
            :param size: the size of the triagle we are currently working on.  Gets smaller with recursion
            :param offset: a random value scaled by the ROUGHNESS variable and size
            :param orientation: a variable to dictate if the triangle is pointside up or pointside down
            */
            int? average;
            List<int?> corners = new List<int?>();
            if (orientation.Equals("UP"))
            {
                corners.Add(triangle.GetHeight(x - size / 2, y - size));  //upper center
                corners.Add(triangle.GetHeight(x - size / 2, y + size));  //lower left
                corners.Add(triangle.GetHeight(x + size / 2 + size, y + size));  //lower right
                average = AveragePoins(corners);
            }
            else
            {
                corners.Add(triangle.GetHeight(x - size - size / 2, y - size));  //upper left
                corners.Add(triangle.GetHeight(x + size / 2, y - size));  //upper right
                corners.Add(triangle.GetHeight(x + size / 2, y + size));  //lower center
                average = AveragePoins(corners);
            }
            if(average + offset > maxHeight)
            {
                maxHeight = average + offset;
            }
            if(average + offset < minHeight)
            {
                minHeight = average + offset;
            }
            triangle.SetHeight(x, y, average + offset);
        }
        private void SetTriangleEdge(int x, int y, int size, int offset, String orientation, String face)
        {
            int? average;
            List<int?> corners = new List<int?>();
            if(face.Equals("EDGE"))
            {
                if(orientation.Equals("LEFT"))
                {
                    corners.Add(triangle.GetHeight(x, y - size));  //upper point
                    corners.Add(triangle.GetHeight(x, y + size));  //lower point
                    corners.Add(triangle.GetHeight(x - size / 2, y));  //left center
                    corners.Add(triangle.GetHeight(x + size / 2, y));  //right center
                    average = AveragePoins(corners);
                }
                else
                {
                    corners.Add(triangle.GetHeight(x - size, y - size));  //upper point
                    corners.Add(triangle.GetHeight(x + size, y + size));  //lower point
                    corners.Add(triangle.GetHeight(x - size / 2, y));  //left center
                    corners.Add(triangle.GetHeight(x + size / 2, y));  //right center
                    average = AveragePoins(corners);
                }
            }
            else
            {
                corners.Add(triangle.GetHeight(x - size / 2, y - size));  //upper center
                corners.Add(triangle.GetHeight(x + size / 2, y + size));  //lower center
                corners.Add(triangle.GetHeight(x - size / 2, y));  //left point
                corners.Add(triangle.GetHeight(x + size / 2, y));  //right point
                average = AveragePoins(corners);
            }
            if (average + offset > maxHeight)
            {
                maxHeight = average + offset;
            }
            if (average + offset < minHeight)
            {
                minHeight = average + offset;
            }
            triangle.SetHeight(x, y, average + offset);
        }
        private int? AveragePoins(List<int?> values)
        {
            /*
            inds the average of a list of values
            ignores a value if it is null
            :param vales: a list of height values to be averaged
            */
            int? total = 0;
            int count = 0;
            foreach(int? value in values)
            {
                if(value != null)
                {
                    total += value;
                    count++;
                }
            }
            return total / count;
        }
        public void BuildSquare(SquareMap squareMap)
        {
            this.squareMap = squareMap;
            fullSize = squareMap.size;
            SetSquareCorners();
            DiamondSquare(fullSize);
        }
        private void SetSquareCorners()
        {
            if (squareMap.GetHeight(0, 0) == null)
            {
                squareMap.SetHeight(0, 0, rng.Next(fullSize * -2, fullSize * 2));
            }
            if (squareMap.GetHeight(fullSize, 0) == null)
            {
                squareMap.SetHeight(fullSize, 0, rng.Next(fullSize * -2, fullSize * 2));
            }
            if (squareMap.GetHeight(0, fullSize) == null)
            {
                squareMap.SetHeight(0, fullSize, rng.Next(fullSize * -2, fullSize * 2));
            }
            if (squareMap.GetHeight(fullSize, fullSize) == null)
            {
                squareMap.SetHeight(fullSize, fullSize, rng.Next(fullSize * -2, fullSize * 2));
            }
        }
        private void DiamondSquare(int size)
        {
            int half = size / 2;
            int scale = roughness * size;
            if (half >= 1)
            {
                for (int y = half; y < fullSize; y += size)
                {
                    for (int x = half; x < fullSize; x += size)
                    {
                        if (squareMap.GetHeight(x, y) == null)
                        {
                            SetSquareSquare(x, y, half, (int)(rng.NextDouble() * scale * 2 - scale));
                        }
                    }
                }
                for (int y = 0; y <= fullSize; y += half)
                {
                    for (int x = (y + half) % size; x <= fullSize; x += size)
                    {
                        if (squareMap.GetHeight(x, y) == null)
                        {
                            SetSquareDiamond(x, y, half, (int)(rng.NextDouble() * scale * 2 - scale));
                        }
                    }
                }
                DiamondSquare(half);
            }
        }
        private void SetSquareSquare(int x, int y, int size, int offset)
        {
            int? average;
            List<int?> corners = new List<int?>();
            corners.Add(squareMap.GetHeight(x - size, y - size));  //upper left
            corners.Add(squareMap.GetHeight(x + size, y - size));  //upper right
            corners.Add(squareMap.GetHeight(x - size, y + size));  //lower left
            corners.Add(squareMap.GetHeight(x + size, y + size));  //lower right
            average = AveragePoins(corners);
            if (average + offset > maxHeight)
            {
                maxHeight = average + offset;
            }
            if (average + offset < minHeight)
            {
                minHeight = average + offset;
            }
            squareMap.SetHeight(x, y, average + offset);
        }
        private void SetSquareDiamond(int x, int y, int size, int offset)
        {
            int? average;
            List<int?> corners = new List<int?>();
            corners.Add(squareMap.GetHeight(x, y - size));  //top
            corners.Add(squareMap.GetHeight(x, y + size));  //bottom
            corners.Add(squareMap.GetHeight(x - size, y));  //left
            corners.Add(squareMap.GetHeight(x + size, y));  //right
            average = AveragePoins(corners);
            if (average + offset > maxHeight)
            {
                maxHeight = average + offset;
            }
            if (average + offset < minHeight)
            {
                minHeight = average + offset;
            }
            squareMap.SetHeight(x, y, average + offset);
        }
        public int? GetColor(int x, int y)
        {
            double? height = squareMap.GetHeight(x, y);
            height = (height - minHeight) / (maxHeight - minHeight) * 255;
            return (int)height;
        }
    }
}