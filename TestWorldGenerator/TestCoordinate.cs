﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Neo4jClient;
using Neo4jClient.Transactions;
using NUnit.Framework;
using WorldGenerator;
using WorldGenerator.Graph;

namespace TestWorldGenerator
{
    [TestFixture]
    public class TestCoordinate
    {
        private ITransactionalGraphClient _graphClient;
        private CoordinateController _coordController;

        [OneTimeSetUp]
        public void CreateDBConnection()
        {
            _graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "admin");
            _graphClient.Connect();
        }

        [SetUp]
        public void InstantiateTestMethodSpace()
        {
            _graphClient.BeginTransaction();
            _coordController = new CoordinateController(_graphClient);
        }

        [TearDown]
        public void tearDown()
        {
            _graphClient.EndTransaction();
        }

        [Test]
        public void TestCoordinateExists()
        {
            _coordController.CreateCoordinate(0, 0, 0);
            Assert.True(_coordController.CoordinateExists(0, 0, 0));
        }

        [Test]
        public void TestCreateCoordinates()
        {
            _coordController.CreateCoordinate(0, 0, 0);
            _coordController.CreateCoordinate(0, 0, 0);

            var nodes = _coordController.GetCoordinates();
            Assert.AreEqual(1, nodes.Count);
            Assert.AreEqual(0, nodes[0].R);
            Assert.AreEqual(0, nodes[0].P);
            Assert.AreEqual(0, nodes[0].Q);
        }

        [Test]
        public void TestGetCoordinate()
        {
            _coordController.CreateCoordinate(0, 0, 0);
            CoordinateNode node = _coordController.GetCoordinate(0, 0, 0);
            Assert.AreEqual(0, node.P);
            Assert.AreEqual(0, node.Q);
            Assert.AreEqual(0, node.R);
        }

        [Test]
        public void TestCoordinatesEquals()
        {
            CoordinateNode coord1 = _coordController.CreateCoordinate(0, 0, 0);
            CoordinateNode coord2 = _coordController.CreateCoordinate(0, 0, 0);
            CoordinateNode notEqual = _coordController.CreateCoordinate(0, 0, 1);

            Assert.False(notEqual.Equals(coord1));
            Assert.True(coord1.Equals(coord2));
            Assert.True(coord2.Equals(coord1));
        }

        [Test]
        public void TestCoordinatesHashCode()
        {
            CoordinateNode coord1 = _coordController.CreateCoordinate(0, 0, 0);
            CoordinateNode coord2 = _coordController.CreateCoordinate(0, 0, 0);

            Assert.AreEqual(coord1.GetHashCode(), coord2.GetHashCode());
        }
    }
}