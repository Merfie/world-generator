﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace TestWorldGenerator
{
    [TestFixture]
    public class TestReflection
    {
        [Test]
        public void TestReflectionWithUntypedList()
        {
            Dictionary<string, object> items = new Dictionary<string, object>();
            const string inputString = "index 0 is a string";
            const int inputInt = 234;
            items.Add("string", inputString);
            items.Add("int", inputInt);

            Assert.IsInstanceOf(typeof(string), items["string"]);
            Assert.IsInstanceOf(typeof(int), items["int"]);
        }
    }
}