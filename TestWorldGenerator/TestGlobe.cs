﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using GlobeGenerator.Globe;
using NUnit.Framework;

namespace TestWorldGenerator
{
    [TestFixture]
    public class TestGlobe
    {
        private GlobeBuilder _globeBuilder;
        private Stopwatch _stopwatch;

        [SetUp]
        public void CreateCoordinateMapper()
        {
            _globeBuilder = new GlobeBuilder(4);
            _stopwatch = new Stopwatch();
        }

        [Test]
        public void TestCreateFaceCreatesAllCoordinates()
        {
            Globe globe = _globeBuilder.CreateFaces();

            Assert.NotNull(globe.GetCoordinate(0, 0, 0));
            Assert.NotNull(globe.GetCoordinate(0, 1, 0));
            Assert.NotNull(globe.GetCoordinate(0, 0, 1));
            Assert.NotNull(globe.GetCoordinate(0, 2, 0));
            Assert.NotNull(globe.GetCoordinate(0, 1, 1));
            Assert.NotNull(globe.GetCoordinate(0, 0, 2));
            Assert.NotNull(globe.GetCoordinate(0, 3, 0));
            Assert.NotNull(globe.GetCoordinate(0, 2, 1));
            Assert.NotNull(globe.GetCoordinate(0, 1, 2));
            Assert.NotNull(globe.GetCoordinate(0, 0, 3));
        }

        [Test]
        public void TestSettingCoordinateAttribute()
        {
            Globe globe = _globeBuilder.CreateFaces();

            int expectedHeight = 40;
            CoordinateValue corner = globe.GetCoordinate(0, 0, 0);

            corner.SetRawHeight(expectedHeight);

            CoordinateValue alsoCorner = globe.GetCoordinate(1, 0, 0);

            Assert.AreEqual(expectedHeight, alsoCorner.GetRawHeight());
        }

        [Test]
        public void TestGetNeighborsForCoordinates()
        {
            Globe globe = _globeBuilder.CreateFaces();

            HashSet<CoordinateValue> centerNeighbors = globe.GetNeighbors(new CoordinateKey(0, 1, 1));

            Assert.AreEqual(6, centerNeighbors.Count);

            HashSet<CoordinateValue> cornerNeighbors = globe.GetNeighbors(new CoordinateKey(0, 0, 0));
            Assert.AreEqual(5, cornerNeighbors.Count);

            HashSet<CoordinateValue> edgeNeigbors = globe.GetNeighbors(new CoordinateKey(0, 1, 0));
            Assert.AreEqual(6, edgeNeigbors.Count);

            var southPoleNeighbors = globe.GetNeighbors(new CoordinateKey(19, 0, 0));
            Assert.AreEqual(5, southPoleNeighbors.Count);
        }

        [Test]
        public void TestVectorsAreCreatedForAllCoordinates()
        {
            Globe globe = _globeBuilder.CreateFaces();

            foreach (Face face in globe.GetFaces())
            {
                foreach (var coordinateValue in face.GetCoordinates())
                {
                    Assert.NotNull(coordinateValue.GetVector().X);
                    Assert.NotNull(coordinateValue.GetVector().Y);
                    Assert.NotNull(coordinateValue.GetVector().Z);
                }
            }
        }

        [Test]
        public void TestTrianglesAreValidKeys()
        {
            Globe globe = _globeBuilder.CreateFaces();

            foreach (Face face in globe.GetFaces())
            {
                foreach (Triangle triangle in face.GetTriangles())
                {
                    Assert.IsTrue(_globeBuilder.IsValidCoordinate(triangle.GetTop()));
                    Assert.IsTrue(_globeBuilder.IsValidCoordinate(triangle.GetLeft()));
                    Assert.IsTrue(_globeBuilder.IsValidCoordinate(triangle.GetRight()));

                }
            }
        }

        [Test]
        public void TestDirections()
        {
            CoordinateKey home = new CoordinateKey(0, 1, 1);

            Assert.AreEqual(new CoordinateKey(0, 2, 0), GlobeBuilder.QwardAcross(home));
            Assert.AreEqual(new CoordinateKey(0, 2, 1), GlobeBuilder.QwardPositive(home));
            Assert.AreEqual(new CoordinateKey(0, 0, 1), GlobeBuilder.QwardNegative(home));
            Assert.AreEqual(new CoordinateKey(0, 0, 2), GlobeBuilder.RwardAcross(home));
            Assert.AreEqual(new CoordinateKey(0, 1, 2), GlobeBuilder.RwardPositive(home));
            Assert.AreEqual(new CoordinateKey(0, 1, 0), GlobeBuilder.RwardNegative(home));
        }

        [Test]
        [Ignore("Test this occasionally, make sure it's not getting too slow, runtime: 4.1 seconds")]
        public void TestBuildTrianglesFor256Time()
        {
            _globeBuilder = new GlobeBuilder(256);
            _stopwatch.Start();
            _globeBuilder.CreateFaces();
            _stopwatch.Stop();

            Console.Out.WriteLine($"It took {_stopwatch.Elapsed} seconds to build a 512 edge world");

        }
    }
}