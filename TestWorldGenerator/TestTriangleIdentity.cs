﻿using System;
using System.Collections.Generic;
using GlobeGenerator.Globe;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace TestWorldGenerator
{
//    /*
//        These tests do not have to be run in order, however, they are meant to be read in order.
//
//        The ascii art above each test represents what we have tested so far.
//
//        It doesn't matter what order the tests run in to achieve this coverage
//
//    */
    [TestFixture]
    public class TestTriangleIdentity
    {
        private Globe _globe;
        private int _sideLength;

        [SetUp]
        public void CreateCoordinateMapper()
        {
            _globe = new GlobeBuilder(5).CreateFaces();
            _sideLength = 4;
        }

//        /*
//            /\      /\      /\      /\      /\
//
//
//             0
//        <       ><                                >
//         \  5   /\                                /\
//
//        */
        [Test]
        public void TestTriangleCornerIdentities()
        {
            // Base case is 4, so 0-3
            // Corner 0
            CoordinateValue coord000 = _globe.GetCoordinate(0, 0, 0);
            CoordinateValue coord100 = _globe.GetCoordinate(1, 0, 0);
            CoordinateValue coord200 = _globe.GetCoordinate(2, 0, 0);
            CoordinateValue coord300 = _globe.GetCoordinate(3, 0, 0);
            CoordinateValue coord400 = _globe.GetCoordinate(4, 0, 0);
            Assert.AreEqual(coord000, coord100);
            Assert.AreEqual(coord000, coord200);
            Assert.AreEqual(coord000, coord300);
            Assert.AreEqual(coord000, coord400);

            //corner 1
            CoordinateValue coord030 = _globe.GetCoordinate(0, _sideLength, 0);
            CoordinateValue coord103 = _globe.GetCoordinate(1, 0, _sideLength);
            CoordinateValue coord503 = _globe.GetCoordinate(5, 0, _sideLength);
            CoordinateValue coord600 = _globe.GetCoordinate(6, 0, 0);
            CoordinateValue coord730 = _globe.GetCoordinate(7, _sideLength, 0);
            Assert.AreEqual(coord030, coord103);
            Assert.AreEqual(coord030, coord503);
            Assert.AreEqual(coord030, coord600);
            Assert.AreEqual(coord030, coord730);

            // corner 2
            CoordinateValue coord130 = _globe.GetCoordinate(1, _sideLength, 0);
            CoordinateValue coord703 = _globe.GetCoordinate(7, 0, _sideLength);
            CoordinateValue coord800 = _globe.GetCoordinate(8, 0, 0);
            CoordinateValue coord930 = _globe.GetCoordinate(9, _sideLength, 0);
            CoordinateValue coord203 = _globe.GetCoordinate(2, 0, _sideLength);
            Assert.AreEqual(coord130, coord703);
            Assert.AreEqual(coord130, coord800);
            Assert.AreEqual(coord130, coord930);
            Assert.AreEqual(coord130, coord203);

            // corner 3
            CoordinateValue coord230 = _globe.GetCoordinate(2, _sideLength, 0);
            CoordinateValue coord903 = _globe.GetCoordinate(9, 0, _sideLength);
            CoordinateValue coord1000 = _globe.GetCoordinate(10, 0, 0);
            CoordinateValue coord1130 = _globe.GetCoordinate(11, _sideLength, 0);
            CoordinateValue coord303 = _globe.GetCoordinate(3, 0, _sideLength);
            Assert.AreEqual(coord230, coord903);
            Assert.AreEqual(coord230, coord1000);
            Assert.AreEqual(coord230, coord1130);
            Assert.AreEqual(coord230, coord303);

            // corner 4
            CoordinateValue coord330 = _globe.GetCoordinate(3, _sideLength, 0);
            CoordinateValue coord1103 = _globe.GetCoordinate(11, 0, _sideLength);
            CoordinateValue coord1200 = _globe.GetCoordinate(12, 0, 0);
            CoordinateValue coord1330 = _globe.GetCoordinate(13, _sideLength, 0);
            CoordinateValue coord403 = _globe.GetCoordinate(4, 0, _sideLength);
            Assert.AreEqual(coord330, coord1103);
            Assert.AreEqual(coord330, coord1200);
            Assert.AreEqual(coord330, coord1330);
            Assert.AreEqual(coord330, coord403);

            // corner 5
            CoordinateValue coord003 = _globe.GetCoordinate(0, 0, _sideLength);
            CoordinateValue coord530 = _globe.GetCoordinate(5, _sideLength, 0);
            CoordinateValue coord430 = _globe.GetCoordinate(4, _sideLength, 0);
            CoordinateValue coord1400 = _globe.GetCoordinate(14, 0, 0);
            CoordinateValue coord1303 = _globe.GetCoordinate(13, 0, _sideLength);
            Assert.AreEqual(coord003, coord530);
            Assert.AreEqual(coord003, coord430);
            Assert.AreEqual(coord003, coord1400);
            Assert.AreEqual(coord003, coord1303);

            // corner 6
            CoordinateValue coord630 = _globe.GetCoordinate(6, _sideLength, 0);
            CoordinateValue coord700 = _globe.GetCoordinate(7, 0, 0);
            CoordinateValue coord803 = _globe.GetCoordinate(8, 0, _sideLength);
            CoordinateValue coord1503 = _globe.GetCoordinate(15, 0, _sideLength);
            CoordinateValue coord1630 = _globe.GetCoordinate(16, _sideLength, 0);
            Assert.AreEqual(coord630, coord700);
            Assert.AreEqual(coord630, coord803);
            Assert.AreEqual(coord630, coord1503);
            Assert.AreEqual(coord630, coord1630);

            // corner 7
            CoordinateValue coord830 = _globe.GetCoordinate(8, _sideLength, 0);
            CoordinateValue coord900 = _globe.GetCoordinate(9, 0, 0);
            CoordinateValue coord1003 = _globe.GetCoordinate(10, 0, _sideLength);
            CoordinateValue coord1603 = _globe.GetCoordinate(16, 0, _sideLength);
            CoordinateValue coord1730 = _globe.GetCoordinate(17, _sideLength, 0);
            Assert.AreEqual(coord830, coord900);
            Assert.AreEqual(coord830, coord1003);
            Assert.AreEqual(coord830, coord1603);
            Assert.AreEqual(coord830, coord1730);

            // corner 8
            CoordinateValue coord1030 = _globe.GetCoordinate(10, _sideLength, 0);
            CoordinateValue coord1100 = _globe.GetCoordinate(11, 0, 0);
            CoordinateValue coord1203 = _globe.GetCoordinate(12, 0, _sideLength);
            CoordinateValue coord1703 = _globe.GetCoordinate(17, 0, _sideLength);
            CoordinateValue coord1830 = _globe.GetCoordinate(18, _sideLength, 0);
            Assert.AreEqual(coord1030, coord1100);
            Assert.AreEqual(coord1030, coord1203);
            Assert.AreEqual(coord1030, coord1703);
            Assert.AreEqual(coord1030, coord1830);

            // corner 9
            CoordinateValue coord1230 = _globe.GetCoordinate(12, _sideLength, 0);
            CoordinateValue coord1300 = _globe.GetCoordinate(13, 0, 0);
            CoordinateValue coord1403 = _globe.GetCoordinate(14, 0, _sideLength);
            CoordinateValue coord1803 = _globe.GetCoordinate(18, 0, _sideLength);
            CoordinateValue coord1930 = _globe.GetCoordinate(19, _sideLength, 0);
            Assert.AreEqual(coord1230, coord1300);
            Assert.AreEqual(coord1230, coord1403);
            Assert.AreEqual(coord1230, coord1803);
            Assert.AreEqual(coord1230, coord1930);

            // corner 10
            CoordinateValue coord1430 = _globe.GetCoordinate(14, _sideLength, 0);
            CoordinateValue coord500 = _globe.GetCoordinate(5, 0, 0);
            CoordinateValue coord603 = _globe.GetCoordinate(6, 0, _sideLength);
            CoordinateValue coord1903 = _globe.GetCoordinate(19, 0, _sideLength);
            CoordinateValue coord1530 = _globe.GetCoordinate(15, _sideLength, 0);
            Assert.AreEqual(coord1430, coord500);
            Assert.AreEqual(coord1430, coord603);
            Assert.AreEqual(coord1430, coord1903);
            Assert.AreEqual(coord1430, coord1530);

            // corner 11
            CoordinateValue coord1500 = _globe.GetCoordinate(15, 0, 0);
            CoordinateValue coord1600 = _globe.GetCoordinate(16, 0, 0);
            CoordinateValue coord1700 = _globe.GetCoordinate(17, 0, 0);
            CoordinateValue coord1800 = _globe.GetCoordinate(18, 0, 0);
            CoordinateValue coord1900 = _globe.GetCoordinate(19, 0, 0);
            Assert.AreEqual(coord1500, coord1600);
            Assert.AreEqual(coord1500, coord1700);
            Assert.AreEqual(coord1500, coord1800);
            Assert.AreEqual(coord1500, coord1900);
        }
//
//        /*
//            /\      /\      /\      /\      /\
//           /  \    /                          \
//          /    \  /                            \
//         /    0 \/                              \
//        <-------><                               >
//         \  5   /\                               /\
//
//        */
//
        [Test]
        public void TestTriangle0EdgeIdentities()
        {
            // Triangle 0
            // Right edge
            CoordinateValue coord010 = _globe.GetCoordinate(0, 1, 0);
            CoordinateValue coord101 = _globe.GetCoordinate(1, 0, 1);
            Assert.AreEqual(coord010, coord101);

            CoordinateValue coord020 = _globe.GetCoordinate(0, 2, 0);
            CoordinateValue coord102 = _globe.GetCoordinate(1, 0, 2);
            Assert.AreEqual(coord020, coord102);

            // left edge
            CoordinateValue coord001 = _globe.GetCoordinate(0, 0, 1);
            CoordinateValue coord410 = _globe.GetCoordinate(4, 1, 0);
            Assert.AreEqual(coord001, coord410);

            CoordinateValue coord002 = _globe.GetCoordinate(0, 0, 2);
            CoordinateValue coord420 = _globe.GetCoordinate(4, 2, 0);
            Assert.AreEqual(coord002, coord420);

            // Bottom edge
            CoordinateValue coord012 = _globe.GetCoordinate(0, 1, _sideLength-1);
            CoordinateValue coord521 = _globe.GetCoordinate(5, _sideLength-1, 1);
            Assert.AreEqual(coord012, coord521);

            CoordinateValue coord021 = _globe.GetCoordinate(0, _sideLength-1, 1);
            CoordinateValue coord512 = _globe.GetCoordinate(5, 1, _sideLength-1);
            Assert.AreEqual(coord021, coord512);
        }
//
//        /*
//            /\      /\      /\      /\      /\
//           /..\    /                          \
//          /....\  /                            \
//         /..0...\/                              \
//        <-------->                               >
//         \  5   /\                               /\i//
//        */
        [Test]
        public void TestTriangle0CenterIdentity()
        {
            CoordinateValue coord011 = _globe.GetCoordinate(0, 1, 1);
            Assert.NotNull(coord011);
            Assert.AreEqual("000111111", coord011.GetId());
        }

        [Test]
        public void TestTriangle1CenterIdentity()
        {
            CoordinateValue coord111 = _globe.GetCoordinate(1, 1, 1);
            Assert.NotNull(coord111);
            Assert.AreEqual("111111111", coord111.GetId());
        }

        [Test]
        public void TestTriangle2CenterIdentity()
        {
            CoordinateValue coord211 = _globe.GetCoordinate(2, 1, 1);
            Assert.NotNull(coord211);
            Assert.AreEqual("222111111", coord211.GetId());
        }

        [Test]
        public void TestTriangle3CenterIdentity()
        {
            CoordinateValue coord311 = _globe.GetCoordinate(3, 1, 1);
            Assert.NotNull(coord311);
            Assert.AreEqual("333111111", coord311.GetId());
        }

        [Test]
        public void TestTriangle4CenterIdentity()
        {
            CoordinateValue coord411 = _globe.GetCoordinate(4, 1, 1);
            Assert.NotNull(coord411);
            Assert.AreEqual("444111111", coord411.GetId());
        }

        [Test]
        public void TestTriangle5CenterIdentity()
        {
            CoordinateValue coord511 = _globe.GetCoordinate(5, 1, 1);
            Assert.NotNull(coord511);
            Assert.AreEqual("555111111", coord511.GetId());
        }

        [Test]
        public void TestTriangle6CenterIdentity()
        {
            CoordinateValue coord611 = _globe.GetCoordinate(6, 1, 1);
            Assert.NotNull(coord611);
            Assert.AreEqual("666111111", coord611.GetId());
        }

        [Test]
        public void TestTriangle7CenterIdentity()
        {
            CoordinateValue coord711 = _globe.GetCoordinate(7, 1, 1);
            Assert.NotNull(coord711);
            Assert.AreEqual("777111111", coord711.GetId());
        }

        [Test]
        public void TestTriangle8CenterIdentity()
        {
            CoordinateValue coord811 = _globe.GetCoordinate(8, 1, 1);
            Assert.NotNull(coord811);
            Assert.AreEqual("888111111", coord811.GetId());
        }

        [Test]
        public void TestTriangle9CenterIdentity()
        {
            CoordinateValue coord911 = _globe.GetCoordinate(9, 1, 1);
            Assert.NotNull(coord911);
            Assert.AreEqual("999111111", coord911.GetId());
        }

        [Test]
        public void TestTriangle10CenterIdentity()
        {
            CoordinateValue coord1011 = _globe.GetCoordinate(10, 1, 1);
            Assert.NotNull(coord1011);
            Assert.AreEqual("101010111111", coord1011.GetId());
        }

        [Test]
        public void TestTriangle11CenterIdentity()
        {
            CoordinateValue coord1111 = _globe.GetCoordinate(11, 1, 1);
            Assert.NotNull(coord1111);
            Assert.AreEqual("111111111111", coord1111.GetId());
        }

        [Test]
        public void TestTriangle12CenterIdentity()
        {
            CoordinateValue coord1211 = _globe.GetCoordinate(12, 1, 1);
            Assert.NotNull(coord1211);
            Assert.AreEqual("121212111111", coord1211.GetId());
        }

        [Test]
        public void TestTriangle13CenterIdentity()
        {
            CoordinateValue coord1311 = _globe.GetCoordinate(13, 1, 1);
            Assert.NotNull(coord1311);
            Assert.AreEqual("131313111111", coord1311.GetId());
        }

        [Test]
        public void TestTriangle14CenterIdentity()
        {
            CoordinateValue coord1411 = _globe.GetCoordinate(14, 1, 1);
            Assert.NotNull(coord1411);
            Assert.AreEqual("141414111111", coord1411.GetId());
        }

        [Test]
        public void TestTriangle15CenterIdentity()
        {
            CoordinateValue coord1511 = _globe.GetCoordinate(15, 1, 1);
            Assert.NotNull(coord1511);
            Assert.AreEqual("151515111111", coord1511.GetId());
        }

        [Test]
        public void TestTriangle16CenterIdentity()
        {
            CoordinateValue coord1611 = _globe.GetCoordinate(16, 1, 1);
            Assert.NotNull(coord1611);
            Assert.AreEqual("161616111111", coord1611.GetId());
        }

        [Test]
        public void TestTriangle17CenterIdentity()
        {
            CoordinateValue coord1711 = _globe.GetCoordinate(17, 1, 1);
            Assert.NotNull(coord1711);
            Assert.AreEqual("171717111111", coord1711.GetId());
        }

        [Test]
        public void TestTriangle18CenterIdentity()
        {
            CoordinateValue coord1811 = _globe.GetCoordinate(18, 1, 1);
            Assert.NotNull(coord1811);
            Assert.AreEqual("181818111111", coord1811.GetId());
        }

        [Test]
        public void TestTriangle19CenterIdentity()
        {
            CoordinateValue coord1911 = _globe.GetCoordinate(19, 1, 1);
            Assert.NotNull(coord1911);
            Assert.AreEqual("191919111111", coord1911.GetId());
        }

        [Test]
        public void TestTriangle0EdgeNeighbors()
        {
            /*
                These get the center of each edge to prevent problems with other edge and corner Ids
                NEVER create CoordinateValues like this
                ALWAYS let the Builder create values
                CoordinateValue equality rests on the ID, which is based on the original coordinate
                Hency why we're avoiding other edges and corners
            */
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(0, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(1, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(1, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(0, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(4, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(4, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(0, 2, _sideLength - 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(5, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(5, 1, 2)));
        }
        [Test]
        public void TestTriangle1EdgeNeighbors()
        {
            /*
                These get the center of each edge to prevent problems with other edge and corner Ids
                NEVER create CoordinateValues like this
                ALWAYS let the Builder create values
                CoordinateValue equality rests on the ID, which is based on the original coordinate
                Hency why we're avoiding other edges and corners
            */
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(1, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(2, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(2, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(1, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(0, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(0, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(1, 2, _sideLength - 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(7, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(7, 1, 2)));
        }

        [Test]
        public void TestTriangle2EdgeNeighbors()
        {
            /*
                These get the center of each edge to prevent problems with other edge and corner Ids
                NEVER create CoordinateValues like this
                ALWAYS let the Builder create values
                CoordinateValue equality rests on the ID, which is based on the original coordinate
                Hency why we're avoiding other edges and corners
            */
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(2, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(3, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(3, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(2, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(1, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(1, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(2, 2, _sideLength - 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(9, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(9, 1, 2)));
        }

        [Test]
        public void TestTriangle3EdgeNeighbors()
        {
            /*
                These get the center of each edge to prevent problems with other edge and corner Ids
                NEVER create CoordinateValues like this
                ALWAYS let the Builder create values
                CoordinateValue equality rests on the ID, which is based on the original coordinate
                Hency why we're avoiding other edges and corners
            */
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(3, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(4, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(4, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(3, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(2, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(2, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(3, 2, _sideLength - 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(11, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(11, 1, 2)));
        }

        [Test]
        public void TestTriangle4EdgeNeighbors()
        {
            /*
                These get the center of each edge to prevent problems with other edge and corner Ids
                NEVER create CoordinateValues like this
                ALWAYS let the Builder create values
                CoordinateValue equality rests on the ID, which is based on the original coordinate
                Hence why we're avoiding other edges and corners
            */
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(4, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(0, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(0, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(4, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(3, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(3, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(4, 2, _sideLength - 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(13, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(13, 1, 2)));
        }

        [Test]
        public void TestTriangle5EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(5, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(14, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(14, 2, 1)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(5, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(6, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(6, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(5, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(0, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(0, 2, 1)));
        }

        [Test]
        public void TestTriangle6EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(6, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(7, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(7, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(6, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(5, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(5, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(6, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(15, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(15, 2, 1)));
        }

        [Test]
        public void TestTriangle7EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(7, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(6, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(6, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(7, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(8, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(8, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(7, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(1, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(1, 2, 1)));
        }

        [Test]
        public void TestTriangle8EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(8, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(9, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(9, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(8, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(7, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(7, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(8, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(16, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(16, 2, 1)));
        }

        [Test]
        public void TestTriangle9EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(9, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(8, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(8, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(9, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(10, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(10, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(9, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(2, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(2, 2, 1)));
        }

        [Test]
        public void TestTriangle10EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(10, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(11, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(11, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(10, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(9, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(9, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(10, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(17, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(17, 2, 1)));
        }

        [Test]
        public void TestTriangle11EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(11, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(10, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(10, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(11, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(12, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(12, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(11, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(3, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(3, 2, 1)));
        }

        [Test]
        public void TestTriangle12EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(12, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(13, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(13, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(12, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(11, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(11, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(12, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(18, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(18, 2, 1)));
        }

        [Test]
        public void TestTriangle13EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(13, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(12, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(12, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(13, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(14, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(14, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(13, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(4, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(4, 2, 1)));
        }

        [Test]
        public void TestTriangle14EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeghbors = _globe.GetNeighbors(new CoordinateKey(14, 2, 0));
            Assert.AreEqual(6, rightEdgeNeghbors.Count);
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(5, 2, 1)));
            Assert.True(rightEdgeNeghbors.Contains(new CoordinateValue(5, 1, 1)));

            HashSet<CoordinateValue> leftEdgeNeigbors = _globe.GetNeighbors(new CoordinateKey(14, 0, 2));
            Assert.AreEqual(6, leftEdgeNeigbors.Count);
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(13, 1, 1)));
            Assert.True(leftEdgeNeigbors.Contains(new CoordinateValue(13, 1, 2)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(14, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(19, 1, 2)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(19, 2, 1)));
        }

        [Test]
        public void TestTriangle15EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(15, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(19, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(19, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(15, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(16, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(16, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(15, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(6, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(6, 1, 2)));
        }

        [Test]
        public void TestTriangle16EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(16, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(15, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(15, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(16, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(17, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(17, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(16, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(8, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(8, 1, 2)));
        }

        [Test]
        public void TestTriangle17EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(17, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(16, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(16, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(17, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(18, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(18, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(17, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(10, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(10, 1, 2)));
        }

        [Test]
        public void TestTriangle18EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(18, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(17, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(17, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(18, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(19, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(19, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(18, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(12, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(12, 1, 2)));
        }

        [Test]
        public void TestTriangle19EdgeNeighbors()
        {
            HashSet<CoordinateValue> rightEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(19, 2, 0));
            Assert.AreEqual(6, rightEdgeNeighbors.Count);
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(18, 1, 1)));
            Assert.True(rightEdgeNeighbors.Contains(new CoordinateValue(18, 1, 2)));

            HashSet<CoordinateValue> leftEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(19, 0, 2));
            Assert.AreEqual(6, leftEdgeNeighbors.Count);
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(15, 1, 1)));
            Assert.True(leftEdgeNeighbors.Contains(new CoordinateValue(15, 2, 1)));

            HashSet<CoordinateValue> floorEdgeNeighbors = _globe.GetNeighbors(new CoordinateKey(19, 2, 2));
            Assert.AreEqual(6, floorEdgeNeighbors.Count);
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(14, 2, 1)));
            Assert.True(floorEdgeNeighbors.Contains(new CoordinateValue(14, 1, 2)));
        }
    }
}