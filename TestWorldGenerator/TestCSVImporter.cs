﻿using System;
using System.Diagnostics;
using Neo4jClient;
using Neo4jClient.Transactions;
using NUnit.Framework;
using WorldGenerator;
using WorldGenerator.Graph;

namespace TestWorldGenerator
{
    [TestFixture]
    public class TestCSVImporter
    {
        private CSVImporter _csvImporter;
        private CoordinateController _controller;
        private GraphClient _graphClient;

        [OneTimeSetUp]
        public void CreateDBConnection()
        {
            _graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "admin");
            _graphClient.Connect();
            _csvImporter = new CSVImporter(_graphClient);
            _controller = new CoordinateController(_graphClient);
        }

        [SetUp]
        public void CreateImporter()
        {
            _graphClient.BeginTransaction();
        }

        [TearDown]
        public void tearDown()
        {
            _graphClient.EndTransaction();

        }

        [Test]
        [Ignore("This cannot run in a transaction either, need to figure out a better way to test this, could just delete everything")]
        public void TestImportNodeCSV()
        {
            _csvImporter.ImportNodes("node_test.csv");

            Assert.AreEqual(6, _controller.GetCoordinates().Count);

            Assert.True(_controller.CoordinateExists(0, 0, 0));
            Assert.True(_controller.CoordinateExists(0, 1, 0));
            Assert.True(_controller.CoordinateExists(0, 0, 1));
            Assert.True(_controller.CoordinateExists(0, 2, 0));
            Assert.True(_controller.CoordinateExists(0, 1, 1));
            Assert.True(_controller.CoordinateExists(0, 0, 2));

        }

        [Test]
        [Ignore("These cannot run within a transaction, but will fill a DB past deletion capabilities")]
        public void TestTimeToImport256World()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            _csvImporter.ImportNodes("nodes-256.csv");
            watch.Stop();

            Console.Out.WriteLine(watch.Elapsed); //~7 seconds
        }

        [Test]
        [Ignore("These cannot run within a transaction, but will fill a DB past deletion capabilities")]
        public void TestTimeToImport512World()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            _csvImporter.ImportNodes("nodes-513.csv");
            watch.Stop();

            Console.Out.WriteLine(watch.Elapsed); //~30 seconds with an arbitrary periodic commit, 1 min without
        }
    }
}