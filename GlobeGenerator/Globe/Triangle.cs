﻿namespace GlobeGenerator.Globe
{
    public class Triangle
    {
        private CoordinateKey _top;
        private CoordinateKey _left;
        private CoordinateKey _right;

        public Triangle(CoordinateKey top, CoordinateKey left, CoordinateKey right)
        {
            _top = top;
            _left = left;
            _right = right;
        }

        public CoordinateKey GetTop()
        {
            return _top;
        }

        public CoordinateKey GetLeft()
        {
            return _left;
        }

        public CoordinateKey GetRight()
        {
            return _right;
        }
    }
}