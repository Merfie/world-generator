﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobeGenerator.Globe
{
    public delegate CoordinateKey Directional(CoordinateKey coord);

    public class GlobeBuilder
    {

        public delegate void IdentityAction(CoordinateValue coordinate);

        private int _sideLength;
        private List<CoordinateValue[][]> _coordinates;
        private static Dictionary<CoordinateKey, HashSet<CoordinateKey>> _cornerNeighbors;
        private List<FaceEdges> _edgeNeighbors;
        private List<List<Triangle>> _triangles;

        public static List<Directional> Directions = new List<Directional>
        {
            QwardPositive,
            RwardPositive,
            QwardNegative,
            RwardNegative,
            QwardAcross,
            RwardAcross
        };


        public GlobeBuilder(int sideLength)
        {
            _sideLength = sideLength;
            _coordinates = new List<CoordinateValue[][]>();
            _cornerNeighbors = new Dictionary<CoordinateKey, HashSet<CoordinateKey>>();
            _edgeNeighbors = new List<FaceEdges>();
            _triangles = new List<List<Triangle>>();

            BuildCoordinatesJaggedArray();
            SeedCornerHelper();
            SeedEdgeNeighborHelper();
            SeedTriangleHolder();
        }

        private void SeedTriangleHolder()
        {
            foreach (var faceIndex in Enumerable.Range(0, 20))
            {
                _triangles.Add(new List<Triangle>());
            }
        }

        public static CoordinateKey QwardPositive(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q+1, source.R);
        }

        public static CoordinateKey RwardPositive(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q, source.R+1);
        }

        public static CoordinateKey QwardNegative(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q-1, source.R);
        }

        public static CoordinateKey RwardNegative(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q, source.R-1);
        }

        public static CoordinateKey QwardAcross(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q+1, source.R-1);
        }

        public static CoordinateKey RwardAcross(CoordinateKey source)
        {
            return new CoordinateKey(source.P, source.Q-1, source.R+1);
        }

        /*
            CreateFaces will create a globe with an orientation and index according to these ascii

            ==Triangle Index==

                /\      /\      /\      /\      /\
               /  \    /  \    /  \    /  \    /  \
              /    \  /    \  /    \  /    \  /    \
             /  0   \/  1   \/  2   \/  3   \/  4   \
            <---------------------------------------->
             \  5   /\  7   /\  9   /\  11  /\  13  /\
              \    /  \    /  \    /  \    /  \    /  \
               \  /    \  /    \  /    \  /    \  /    \
                \/  6   \/  8   \/  10  \/  12  \/  14  \
                <---------------------------------------->
                 \  15  /\  16  /\  17  /\  18  /\  19  /
                  \    /  \    /  \    /  \    /  \    /
                   \  /    \  /    \  /    \  /    \  /
                    \/      \/      \/      \/      \/

             ==Corner Index== (For assembly)

                /\      /\      /\      /\      /\
               /0 \    /0 \    /0 \    /0 \    /0 \
              /    \  /    \  /    \  /    \  /    \
             /      \/      \/      \/      \/      \
           5<-------1-------2-------3-------4-------->5
             \      /\      /\      /\      /\      /\
              \    /  \    /  \    /  \    /  \    /  \
               \  /    \  /    \  /    \  /    \  /    \
                \/      \/      \/      \/      \/      \
              10<-------6-------7-------8-------9-------->10
                 \      /\      /\      /\      /\      /
                  \    /  \    /  \    /  \    /  \    /
                   \  /    \  /    \  /    \  /    \  /
                    \/      \/      \/      \/      \/
                    11      11      11      11      11

                                /\
                               /  \
                              /    \
                             /  0   \
                            <-------->
           (0, 0, sideLength)         (0, sideLength, 0)

          (15, sideLength, 0)        (15, 0 sideLength)
                            <-------->
                             \  15  /
                              \    /
                               \  /
                                \/


            Note that the middle belt starts upside down, if we're reading from left to right
            This method will build the middle belt accordingly, attached to face 0 at face 5
        */
        public Globe CreateFaces()
        {
            // Build raw faces, no stitching
            for (int i = 0; i < 20; i++)
            {
                CreateFace(i);
            }

            EstablishEdges();

            // fuck it, manually hook up corners, it's constant
            EstablishCorners();

            PopulateVectors();

            return new Globe(GetCoordinates(), _cornerNeighbors, _edgeNeighbors, MaxSideLength());
        }

        private double GetDistance(int sideIndex)
        {
            // the cast is to avoid the fraction loss I guess
            return (double)sideIndex / MaxSideLength();
        }

        private void PopulateVectors()
        {
            foreach (var faceIndex in Enumerable.Range(0, 20))
            {
                CoordinateKey topCorner = new CoordinateKey(faceIndex, 0, 0);
                CoordinateKey leftCorner = new CoordinateKey(faceIndex, 0, MaxSideLength());
                CoordinateKey rightCorner = new CoordinateKey(faceIndex, MaxSideLength(), 0);

                // Check if the left edge is calculated
                if (GetCoordinate(RwardPositive(topCorner)).GetVector().IsNull())
                {
                    // This edge is not yet filled in, filling it in
                    // start on the edge instead of the corner to save time, the corners won't change
                    CoordinateKey leftEdgeTracker = RwardPositive(topCorner);
                    while (IsValidCoordinate(leftEdgeTracker) && !IsCorner(leftEdgeTracker))
                    {
                        double distance = GetDistance(leftEdgeTracker.R);
                        Vector vector = CalculateVectorAtDistance(GetCoordinate(topCorner), GetCoordinate(leftCorner), distance);
                        vector = NormalizeVector(vector);
                        GetCoordinate(leftEdgeTracker).SetVector(vector.X, vector.Y, vector.Z);
                        leftEdgeTracker = RwardPositive(leftEdgeTracker);
                    }

                }

                // Check if the right edge is calculated
                if (GetCoordinate(QwardPositive(topCorner)).GetVector().IsNull())
                {
                    //Fill in the right edge
                    CoordinateKey rightEdgeTracker = QwardPositive(topCorner);
                    while (IsValidCoordinate(rightEdgeTracker) && !IsCorner(rightEdgeTracker))
                    {
                        double distance = GetDistance(rightEdgeTracker.Q);
                        Vector vector = CalculateVectorAtDistance(GetCoordinate(topCorner), GetCoordinate(rightCorner),
                            distance);
                        vector = NormalizeVector(vector);
                        GetCoordinate(rightEdgeTracker).SetVector(vector.X, vector.Y, vector.Z);

                        rightEdgeTracker = QwardPositive(rightEdgeTracker);
                    }
                }

                //Check if the floor edge is calculated
                if (GetCoordinate(QwardAcross(leftCorner)).GetVector().IsNull())
                {
                    // fill in floor edge
                    CoordinateKey bottomEdgeTracker = QwardAcross(leftCorner);
                    while (IsValidCoordinate(bottomEdgeTracker) && !IsCorner(bottomEdgeTracker))
                    {
                        double distance = GetDistance(bottomEdgeTracker.Q);
                        Vector vector = CalculateVectorAtDistance(GetCoordinate(leftCorner), GetCoordinate(rightCorner),
                            distance);
                        vector = NormalizeVector(vector);
                        GetCoordinate(bottomEdgeTracker).SetVector(vector.X, vector.Y, vector.Z);

                        bottomEdgeTracker = QwardAcross(bottomEdgeTracker);
                    }
                }

                // Top node and second row edge case for discovering vertex
                CoordinateKey top = new CoordinateKey(faceIndex, 0, 0);
                CoordinateKey secondRowLeft = RwardPositive(top);
                CoordinateKey secondRowRight = QwardPositive(top);
                AddTriangleGoingFloor(faceIndex, top);
                AddTriangleGoingFloor(faceIndex, secondRowLeft);
                AddTriangleGoingFloor(faceIndex, secondRowRight);

                // Fill in the center, we're iterating in a very different way here
                // Instaed of iterating on the natural diagonoal, we're going straight across each row, scanning down
                // Starting at 2 because we know the top corner and first row have been filled in (b/c of edges)
                // Ending before the last row because the floor is already populated
                foreach (int qr in Enumerable.Range(2, MaxSideLength()-2))
                {
                    // Interestingly, this entire loop could be rewritten to use the directional methods
                    // I think this communicates better though
                    CoordinateKey source = new CoordinateKey(faceIndex, 0, qr);
                    CoordinateKey dest = new CoordinateKey(faceIndex, qr, 0);

                    // Adding the triangle info for the ends
                    AddTriangleGoingFloor(faceIndex, source);
                    AddTriangleGoingFloor(faceIndex, dest);

                    // Arbitrarily choosing here, Left to right
                    CoordinateKey centerTracker = QwardAcross(source);

                    // We know source and dest are filled in from above, so populate between them
                    while (IsValidCoordinate(centerTracker) && !centerTracker.Equals(dest))
                    {
                        // Note the difference from the edges in the distance
                        double distance = (double) centerTracker.Q / qr;
                        Vector vector = CalculateVectorAtDistance(GetCoordinate(source), GetCoordinate(dest), distance);
                        vector = NormalizeVector(vector);
                        GetCoordinate(centerTracker).SetVector(vector.X, vector.Y, vector.Z);

                        AddTriangleGoingFloor(faceIndex, centerTracker);
                        AddTriangleGoingTop(faceIndex, centerTracker);

                        centerTracker = QwardAcross(centerTracker);
                    }
                }

                // handle the bottom row edge case
                CoordinateKey floorRowLeftTracker = new CoordinateKey(faceIndex, 1, MaxSideLength()-1);
                while (IsValidCoordinate(floorRowLeftTracker) && !IsCorner(floorRowLeftTracker))
                {
                    AddTriangleGoingTop(faceIndex, floorRowLeftTracker);
                    floorRowLeftTracker = QwardAcross(floorRowLeftTracker);
                }
            }
        }

        private void AddTriangleGoingTop(int faceIndex, CoordinateKey top)
        {
            _triangles[faceIndex].Add(new Triangle(top, RwardNegative(top), QwardNegative(top)));
        }

        private void AddTriangleGoingFloor(int faceIndex, CoordinateKey top)
        {
            _triangles[faceIndex].Add(new Triangle(top, RwardPositive(top), QwardPositive(top)));
        }

        public bool IsCorner(CoordinateKey coordinate)
        {
            return coordinate.Q == 0 && coordinate.R == MaxSideLength() ||
                   coordinate.Q == MaxSideLength() && coordinate.R == 0 ||
                   coordinate.Q == 0 && coordinate.R == 0;
        }

        private Vector NormalizeVector(Vector vector)
        {
            double magnitude = Math.Sqrt(Math.Pow(vector.XNullSafe(), 2) + Math.Pow(vector.YNullSafe(), 2) +
                                         Math.Pow(vector.ZNullSafe(), 2));
            return new Vector(vector.X/magnitude, vector.Y/magnitude, vector.Z/magnitude);
        }

        private Vector CalculateVectorAtDistance(CoordinateValue coord1, CoordinateValue coord2, double distance)
        {
            double x = (1 - distance) * coord1.GetVector().XNullSafe() + distance * coord2.GetVector().XNullSafe();
            double y = (1 - distance) * coord1.GetVector().YNullSafe() + distance * coord2.GetVector().YNullSafe();
            double z = (1 - distance) * coord1.GetVector().ZNullSafe() + distance * coord2.GetVector().ZNullSafe();

            return new Vector(x, y, z);
        }

        private void EstablishEdges()
        {
            // Top
            int upsideDownIndexOffset = 5;
            foreach (FaceIndex faceIndex in TopStripFaceOverlap())
            {
                // TODO: make this loop follow the while() style of the rest of this code
                for (int i = 0; i < _sideLength; i++)
                {
                    // Vertical edges, easy peasy
                    CoordinateKey leftNode = new CoordinateKey(faceIndex.currIndex, i, 0);
                    CoordinateKey rightNode = new CoordinateKey(faceIndex.nextIndex, 0, i);

                    EstablishEdgeIdentity(leftNode, rightNode);
                }
                // stitch together the edges here, do the same with the other overlap functions

                // Bottom to middle's upside-down bottom. Tricky
                // I don't think this is very time efficient, but it communicates better in this case
                // Update the offset to the next index before we do the operation
                CoordinateKey topLowerEdgeNodeTracker = new CoordinateKey(faceIndex.currIndex, 0, _sideLength - 1);
                while (IsValidCoordinate(topLowerEdgeNodeTracker))
                {
                    // Notice the PRQ! That is on purpose!
                    CoordinateKey nextEdgeNodeTracker =
                        new CoordinateKey(upsideDownIndexOffset, topLowerEdgeNodeTracker.R, topLowerEdgeNodeTracker.Q);

                    EstablishEdgeIdentity(topLowerEdgeNodeTracker, nextEdgeNodeTracker);

                    // Move the tracker to the next slot
                    topLowerEdgeNodeTracker = QwardAcross(topLowerEdgeNodeTracker);
                }

                upsideDownIndexOffset += 2;
            }

            // Middle
            foreach (FaceIndex faceIndex in MiddleStripFaceOverlap())
            {
                // all the horizontal edges will be handled by top and bottom
                if (IsOddFace(faceIndex.currIndex))
                {
                    // currindex is odd
                    CoordinateKey currEdgeTracker = new CoordinateKey(faceIndex.currIndex, 0, _sideLength - 1);
                    CoordinateKey nextEdgeTracker = new CoordinateKey(faceIndex.nextIndex, 0, 0);
                    // The extra 'and' here would be redundant, pre-optimizing
                    while (IsValidCoordinate(currEdgeTracker))
                    {
                        EstablishEdgeIdentity(currEdgeTracker, nextEdgeTracker);
                        currEdgeTracker = RwardNegative(currEdgeTracker);
                        nextEdgeTracker = RwardPositive(nextEdgeTracker);
                    }
                }
                else
                {
                    CoordinateKey currEdgeTracker = new CoordinateKey(faceIndex.currIndex, 0, 0);
                    CoordinateKey nextEdgeTracker = new CoordinateKey(faceIndex.nextIndex, _sideLength - 1, 0);

                    while (IsValidCoordinate(currEdgeTracker))
                    {
                        EstablishEdgeIdentity(currEdgeTracker, nextEdgeTracker);
                        currEdgeTracker = QwardPositive(currEdgeTracker);
                        nextEdgeTracker = QwardNegative(nextEdgeTracker);
                    }
                }
            }

            // Bottom
            int middleTriangleIndexOffset = 6;
            foreach (FaceIndex faceIndex in BottomStripFaceOverlap())
            {
                // stitch together the edges here, do the same with the other overlap functions
                for (int i = 0; i < _sideLength; i++)
                {
                    // Vertical edges, easy peasy
                    CoordinateKey leftNode = new CoordinateKey(faceIndex.currIndex, 0, i);
                    CoordinateKey rightNode = new CoordinateKey(faceIndex.nextIndex, i, 0);

                    EstablishEdgeIdentity(leftNode, rightNode);
                }

                // top to middle's bottom. Tricky
                // Update the offset to the next index before we do the operation
                CoordinateKey bottomUpperEdgeNodeTracker = new CoordinateKey(faceIndex.currIndex, 0, _sideLength - 1);
                while (IsValidCoordinate(bottomUpperEdgeNodeTracker))
                {
                    // Notice the PRQ! That is on purpose!
                    CoordinateKey upsideDownNode = new CoordinateKey(
                        middleTriangleIndexOffset, bottomUpperEdgeNodeTracker.R, bottomUpperEdgeNodeTracker.Q);

                    EstablishEdgeIdentity(upsideDownNode, bottomUpperEdgeNodeTracker);

                    // Move the tracker to the next slot
                    bottomUpperEdgeNodeTracker = QwardAcross(bottomUpperEdgeNodeTracker);
                }
                middleTriangleIndexOffset += 2;
            }
        }

        // This method will ensure corners are solid and that they have vectors
        private void EstablishCorners()
        {
            CoordinateKey northPole = new CoordinateKey(0, 0, 0);
            EstablishEdgeIdentity(northPole, new CoordinateKey(1, 0, 0));
            EstablishEdgeIdentity(northPole, new CoordinateKey(2, 0, 0));
            EstablishEdgeIdentity(northPole, new CoordinateKey(3, 0, 0));
            EstablishEdgeIdentity(northPole, new CoordinateKey(4, 0, 0));
            VectorizeCoordinate(northPole, -0.02997109447899471, 0.0, 0.9995507658422011);

            CoordinateKey corner1 = new CoordinateKey(0, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner1, new CoordinateKey(1, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner1, new CoordinateKey(5, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner1, new CoordinateKey(6, 0, 0));
            EstablishEdgeIdentity(corner1, new CoordinateKey(7, MaxSideLength(), 0));
            VectorizeCoordinate(corner1, 0.2628655560595668, 0.8506508083520399, 0.45529649865501465);

            CoordinateKey corner2 = new CoordinateKey(1, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner2, new CoordinateKey(7, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner2, new CoordinateKey(8, 0, 0));
            EstablishEdgeIdentity(corner2, new CoordinateKey(9, MaxSideLength(), 0));
            EstablishEdgeIdentity(corner2, new CoordinateKey(2, 0, MaxSideLength()));
            VectorizeCoordinate(corner2, -0.7366852097826344, 0.5257311121191336, 0.42532540417601994);

            CoordinateKey corner3 = new CoordinateKey(2, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner3, new CoordinateKey(9, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner3, new CoordinateKey(10, 0, 0));
            EstablishEdgeIdentity(corner3, new CoordinateKey(11, MaxSideLength(), 0));
            EstablishEdgeIdentity(corner3, new CoordinateKey(3, 0, MaxSideLength()));
            VectorizeCoordinate(corner3, -0.7366852097826344, -0.5257311121191336, 0.42532540417601994);

            CoordinateKey corner4 = new CoordinateKey(3, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner4, new CoordinateKey(11, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner4, new CoordinateKey(12, 0, 0));
            EstablishEdgeIdentity(corner4, new CoordinateKey(13, MaxSideLength(), 0));
            EstablishEdgeIdentity(corner4, new CoordinateKey(4, 0, MaxSideLength()));
            VectorizeCoordinate(corner4, 0.2628655560595668, -0.8506508083520399, 0.45529649865501465);

            CoordinateKey corner5 = new CoordinateKey(0, 0, MaxSideLength());
            EstablishEdgeIdentity(corner5, new CoordinateKey(5, MaxSideLength(), 0));
            EstablishEdgeIdentity(corner5, new CoordinateKey(4, MaxSideLength(), 0));
            EstablishEdgeIdentity(corner5, new CoordinateKey(14, 0, 0));
            EstablishEdgeIdentity(corner5, new CoordinateKey(13, 0, MaxSideLength()));
            VectorizeCoordinate(corner5, 0.8806219028310346, 0.0, 0.4738196537230676);

            CoordinateKey corner6 = new CoordinateKey(6, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner6, new CoordinateKey(7, 0, 0));
            EstablishEdgeIdentity(corner6, new CoordinateKey(8, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner6, new CoordinateKey(15, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner6, new CoordinateKey(16, MaxSideLength(), 0));
            VectorizeCoordinate(corner6, -0.2628655560595668, 0.8506508083520399, -0.45529649865501465);

            CoordinateKey corner7 = new CoordinateKey(8, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner7, new CoordinateKey(9, 0, 0));
            EstablishEdgeIdentity(corner7, new CoordinateKey(10, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner7, new CoordinateKey(16, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner7, new CoordinateKey(17, MaxSideLength(), 0));
            VectorizeCoordinate(corner7, -0.8806219028310346, 0.0, -0.4738196537230676);

            CoordinateKey corner8 = new CoordinateKey(10, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner8, new CoordinateKey(11, 0, 0));
            EstablishEdgeIdentity(corner8, new CoordinateKey(12, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner8, new CoordinateKey(17, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner8, new CoordinateKey(18, MaxSideLength(), 0));
            VectorizeCoordinate(corner8, -0.2628655560595668, -0.8506508083520399, -0.45529649865501465);

            CoordinateKey corner9 = new CoordinateKey(12, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner9, new CoordinateKey(13, 0, 0));
            EstablishEdgeIdentity(corner9, new CoordinateKey(14, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner9, new CoordinateKey(18, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner9, new CoordinateKey(19, MaxSideLength(), 0));
            VectorizeCoordinate(corner9, 0.7366852097826344, -0.5257311121191336, -0.42532540417601994);

            CoordinateKey corner10 = new CoordinateKey(14, MaxSideLength(), 0);
            EstablishEdgeIdentity(corner10, new CoordinateKey(5, 0, 0));
            EstablishEdgeIdentity(corner10, new CoordinateKey(6, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner10, new CoordinateKey(19, 0, MaxSideLength()));
            EstablishEdgeIdentity(corner10, new CoordinateKey(15, MaxSideLength(), 0));
            VectorizeCoordinate(corner10, 0.7366852097826344, 0.5257311121191336, -0.42532540417601994);

            CoordinateKey corner11 = new CoordinateKey(15, 0, 0);
            EstablishEdgeIdentity(corner11, new CoordinateKey(16, 0, 0));
            EstablishEdgeIdentity(corner11, new CoordinateKey(17, 0, 0));
            EstablishEdgeIdentity(corner11, new CoordinateKey(18, 0, 0));
            EstablishEdgeIdentity(corner11, new CoordinateKey(19, 0, 0));
            VectorizeCoordinate(corner11, 0.02997109447899471, 0.0, -0.9995507658422011);
        }

        private void VectorizeCoordinate(CoordinateKey coord, double x, double y, double z)
        {
            GetCoordinate(coord).SetVector(x, y, z);
        }

        private void EstablishEdgeIdentity(CoordinateKey source, CoordinateKey target)
        {
            UpdateCoordinate(target.P, target.Q, target.R, GetCoordinate(source));
        }

        private void CreateFace(int index)
        {
            for (int q = 0; q < _sideLength; q++)
            {
                for (int r = 0; r < _sideLength-q; r++)
                {
                    AddCoordinate(index, q, r);
                }
            }
        }

        private bool IsOddFace(int face)
        {
            return face % 2 != 0;
        }

        private List<Face> GetCoordinates()
        {
            List<Face> result = new List<Face>();
            foreach (int faceIndex in Enumerable.Range(0, 20))
            {
                result.Add(new Face(_coordinates[faceIndex], _sideLength, _triangles[faceIndex]));
            }

            return result;
        }

        private void UpdateCoordinate(int p, int q, int r, CoordinateValue newCoord)
        {
            _coordinates[p][q][r] = newCoord;
        }

        private CoordinateValue GetCoordinate(CoordinateKey coord)
        {
            return _coordinates[coord.P][coord.Q][coord.R];
        }

        private IEnumerable<FaceIndex> OverlapIterator(int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                int nextFace = i + 1;
                if (nextFace == end)
                {
                    nextFace = start;
                }

                int afterNext = nextFace + 1;
                if (afterNext == end)
                {
                    afterNext = start;
                }

                yield return new FaceIndex(i, nextFace, afterNext);
            }
        }

        private IEnumerable<FaceIndex> TopStripFaceOverlap()
        {
            return OverlapIterator(0, 5);
        }

        private IEnumerable<FaceIndex> MiddleStripFaceOverlap()
        {
            return OverlapIterator(5, 15);
        }

        private IEnumerable<FaceIndex> BottomStripFaceOverlap()
        {
            return OverlapIterator(15, 20);
        }

        //I have to be doing something wrong here, but an Enumerator always starts at 0. Weird.
        private IEnumerator<FaceIndex> FaceCounter(IEnumerable<FaceIndex> faceOverlap)
        {
            var result = faceOverlap.GetEnumerator();
            result.MoveNext();
            return result;
        }

        private void BuildCoordinatesJaggedArray()
        {
            for (int i = 0; i < 20; i++)
            {
                CoordinateValue[][] face = new CoordinateValue[_sideLength][];
                for (int j = 0; j < _sideLength; j++)
                {
                    face[j] = new CoordinateValue[_sideLength-j];
                }
                _coordinates.Add(face);

            }
        }

        // Well this is awful, but the information has to live somewhere and this will allow creation to read simple
        private void SeedCornerHelper()
        {
            int maxSizeIndex = MaxSideLength();

            var northPoleNeighbors = new HashSet<CoordinateKey>
                                        {
                                            new CoordinateKey(0, 0, 1),
                                            new CoordinateKey(0, 1, 0),
                                            new CoordinateKey(1, 1, 0),
                                            new CoordinateKey(2, 1, 0),
                                            new CoordinateKey(3, 1, 0),
                                        };
            var southPoleNeighbors = new HashSet<CoordinateKey>
                                        {
                                            new CoordinateKey(15, 0, 1),
                                            new CoordinateKey(15, 1, 0),
                                            new CoordinateKey(16, 0, 1),
                                            new CoordinateKey(17, 0, 1),
                                            new CoordinateKey(18, 0, 1),
                                        };

            // Top
            int topOffset = 6;
            foreach (FaceIndex faceIndex in TopStripFaceOverlap())
            {
                var topStitchNeighbors = new HashSet<CoordinateKey>
                {
                    new CoordinateKey(faceIndex.currIndex, maxSizeIndex - 1, 1),
                    new CoordinateKey(faceIndex.currIndex, maxSizeIndex - 1, 0),
                    new CoordinateKey(faceIndex.nextIndex, 1, maxSizeIndex - 1),
                    new CoordinateKey(topOffset, 1, 0),
                    new CoordinateKey(topOffset, 0, 1),
                };
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, 0, 0), northPoleNeighbors);
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, maxSizeIndex, 0), topStitchNeighbors);
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.nextIndex, 0, maxSizeIndex), topStitchNeighbors);

                topOffset += 2;
            }

            // Middle
            IEnumerator<FaceIndex> topOffsetEnum = FaceCounter(TopStripFaceOverlap());
            IEnumerator<FaceIndex> bottomOffsetEnum = FaceCounter(BottomStripFaceOverlap());
            foreach (FaceIndex faceIndex in MiddleStripFaceOverlap())
            {
                FaceIndex currOddTopOffset = topOffsetEnum.Current;

                FaceIndex currOddBottomOffset = bottomOffsetEnum.Current;
                if (IsOddFace(faceIndex.currIndex))
                {
                    var topThreeOddTriangleStitch = new HashSet<CoordinateKey>
                    {
                        new CoordinateKey(faceIndex.currIndex, 1, maxSizeIndex-1),
                        new CoordinateKey(faceIndex.nextIndex, 0, 1),
                        new CoordinateKey(faceIndex.nextIndex, 1, 0),
                        new CoordinateKey(currOddTopOffset.currIndex, maxSizeIndex - 1, 1),
                        new CoordinateKey(currOddTopOffset.nextIndex, 1, maxSizeIndex-1)
                    };

                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, 0, maxSizeIndex), topThreeOddTriangleStitch);
                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.nextIndex, 0, 0), topThreeOddTriangleStitch);
                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.afterNextIndex, maxSizeIndex, 0), topThreeOddTriangleStitch);


                }
                else // even face
                {
                    var bottomThreeTriangleStitch = new HashSet<CoordinateKey>
                    {
                        new CoordinateKey(faceIndex.currIndex, 1, 0),
                        new CoordinateKey(faceIndex.currIndex, 0, 1),
                        new CoordinateKey(currOddBottomOffset.currIndex, 1, maxSizeIndex-1),
                        new CoordinateKey(currOddBottomOffset.currIndex, 0, maxSizeIndex-1),
                        new CoordinateKey(currOddBottomOffset.nextIndex, maxSizeIndex-1, 1)
                    };

                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, maxSizeIndex, 0), bottomThreeTriangleStitch);
                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.nextIndex, 0, 0), bottomThreeTriangleStitch);
                    _cornerNeighbors.Add(new CoordinateKey(faceIndex.afterNextIndex, 0, maxSizeIndex), bottomThreeTriangleStitch);

                    topOffsetEnum.MoveNext();
                    bottomOffsetEnum.MoveNext();
                }
            }
            topOffsetEnum.Dispose();
            bottomOffsetEnum.Dispose();

            // bottom
            // This one got ugly. The bottom relates to the middle in a more offset way than the top
            IEnumerator<FaceIndex> bottomMiddleOffset = FaceCounter(MiddleStripFaceOverlap());
            foreach (FaceIndex faceIndex in BottomStripFaceOverlap())
            {
                bottomMiddleOffset.MoveNext();
                int bottomOffset = bottomMiddleOffset.Current.nextIndex;
                var bottomStitchNeighbors = new HashSet<CoordinateKey>
                {
                    new CoordinateKey(faceIndex.currIndex, 1, maxSizeIndex - 1),
                    new CoordinateKey(faceIndex.currIndex, 0, maxSizeIndex - 1),
                    new CoordinateKey(faceIndex.nextIndex, maxSizeIndex - 1, 1),
                    new CoordinateKey(bottomOffset, 1, 0),
                    new CoordinateKey(bottomOffset, 0, 1),
                };
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, 0, 0), southPoleNeighbors);
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.currIndex, maxSizeIndex, 0), bottomStitchNeighbors);
                _cornerNeighbors.Add(new CoordinateKey(faceIndex.nextIndex, 0, maxSizeIndex), bottomStitchNeighbors);

                bottomMiddleOffset.MoveNext();
            }
            bottomMiddleOffset.Dispose();
        }

        private void SeedEdgeNeighborHelper()
        {
            // initialize the objects to not worry about that
            foreach (int faceIndex in Enumerable.Range(0, 20))
            {
                _edgeNeighbors.Add(new FaceEdges());
            }

            //Top
            int topFloorOffset = 5;
            foreach (FaceIndex faceIndex in TopStripFaceOverlap())
            {
                var middleFaceIndex = topFloorOffset;
                List<Directional> rightEdgeDirectionals = new List<Directional>
                {
                    QwardNegative,
                    QwardPositive,
                    RwardPositive,
                    RwardAcross,
                    home => QwardAcross(new CoordinateKey(faceIndex.nextIndex, home.R, home.Q)),
                    home => QwardPositive(new CoordinateKey(faceIndex.nextIndex, home.R, home.Q))
                };

                List<Directional> leftEdgeDirectionals = new List<Directional>
                {
                    RwardNegative,
                    QwardAcross,
                    QwardPositive,
                    RwardPositive,
                    home => RwardAcross(new CoordinateKey(faceIndex.currIndex, home.R, home.Q)),
                    home => RwardPositive(new CoordinateKey(faceIndex.currIndex, home.R, home.Q))
                };

                List<Directional> floorTopEdgeDirectionals = new List<Directional>
                {
                    RwardAcross,
                    QwardNegative,
                    RwardNegative,
                    QwardAcross,
                    home => QwardNegative(new CoordinateKey(middleFaceIndex, home.R, home.Q)),
                    home => RwardNegative(new CoordinateKey(middleFaceIndex, home.R, home.Q)),
                };

                List<Directional> floorMiddleEdgeDirectionals = new List<Directional>
                {
                    RwardAcross,
                    QwardAcross,
                    RwardNegative,
                    QwardNegative,
                    home => RwardNegative(new CoordinateKey(faceIndex.currIndex, home.R, home.Q)),
                    home => QwardNegative(new CoordinateKey(faceIndex.currIndex, home.R, home.Q))
                };

                FaceEdges currFace = _edgeNeighbors[faceIndex.currIndex];
                FaceEdges nextFace = _edgeNeighbors[faceIndex.nextIndex];
                FaceEdges middleFace = _edgeNeighbors[middleFaceIndex];

                currFace.SetRightEdge(rightEdgeDirectionals);
                currFace.SetFloorEdge(floorTopEdgeDirectionals);

                nextFace.SetLeftEdge(leftEdgeDirectionals);

                middleFace.SetFloorEdge(floorMiddleEdgeDirectionals);

                topFloorOffset += 2;
            }

            // Middle
            foreach (FaceIndex faceIndex in MiddleStripFaceOverlap())
            {
                if (IsOddFace(faceIndex.currIndex))
                {
                    List<Directional> oddLeftEdgeDirectionals = new List<Directional>
                    {
                        RwardNegative,
                        RwardPositive,
                        QwardAcross,
                        QwardPositive,
                        home => QwardAcross(new CoordinateKey(faceIndex.nextIndex, 0, MaxSideLength() - home.R)),
                        home => QwardPositive(new CoordinateKey(faceIndex.nextIndex, 0, MaxSideLength() - home.R))
                    };
                    List<Directional> evenLeftEdgeDirectionals = new List<Directional>
                    {
                        RwardNegative,
                        RwardPositive,
                        QwardAcross,
                        QwardPositive,
                        home => QwardAcross(new CoordinateKey(faceIndex.currIndex, 0, MaxSideLength() - home.R)),
                        home => QwardPositive(new CoordinateKey(faceIndex.currIndex, 0, MaxSideLength() - home.R))
                    };
                    _edgeNeighbors[faceIndex.currIndex].SetLeftEdge(oddLeftEdgeDirectionals);
                    _edgeNeighbors[faceIndex.nextIndex].SetLeftEdge(evenLeftEdgeDirectionals);
                }
                else
                {
                    List<Directional> evenRightEdgeDirectionals = new List<Directional>
                    {
                        QwardNegative,
                        RwardAcross,
                        RwardPositive,
                        QwardPositive,
                        home => RwardAcross(new CoordinateKey(faceIndex.nextIndex, MaxSideLength() - home.Q, 0)),
                        home => RwardPositive(new CoordinateKey(faceIndex.nextIndex, MaxSideLength() - home.Q, 0))
                    };

                    List<Directional> oddRightEdgeDirectionals = new List<Directional>
                    {
                        QwardNegative,
                        RwardAcross,
                        RwardPositive,
                        QwardPositive,
                        home => RwardAcross(new CoordinateKey(faceIndex.currIndex, MaxSideLength() - home.Q, 0)),
                        home => RwardPositive(new CoordinateKey(faceIndex.currIndex, MaxSideLength() - home.Q, 0))
                    };

                    _edgeNeighbors[faceIndex.currIndex].SetRightEdge(evenRightEdgeDirectionals);
                    _edgeNeighbors[faceIndex.nextIndex].SetRightEdge(oddRightEdgeDirectionals);
                }
            }

            // Bottom
            int bottomMiddleOffset = 6;
            foreach (FaceIndex faceIndex in BottomStripFaceOverlap())
            {
                var offset = bottomMiddleOffset;
                List<Directional> bottomFloorEdgeDirectionals = new List<Directional>
                {
                    RwardAcross,
                    QwardAcross,
                    RwardNegative,
                    QwardNegative,
                    home => RwardNegative(new CoordinateKey(offset, home.R, home.Q)),
                    home => QwardNegative(new CoordinateKey(offset, home.R, home.Q))
                };

                List<Directional> middleFloorEdgeDirectionals = new List<Directional>
                {
                    RwardAcross,
                    QwardAcross,
                    RwardNegative,
                    QwardNegative,
                    home => RwardNegative(new CoordinateKey(faceIndex.currIndex, home.R, home.Q)),
                    home => QwardNegative(new CoordinateKey(faceIndex.currIndex, home.R, home.Q))
                };

                List<Directional> bottomLeftEdgeDirectionals = new List<Directional>
                {
                    RwardPositive,
                    RwardNegative,
                    QwardPositive,
                    QwardAcross,
                    home => RwardAcross(new CoordinateKey(faceIndex.nextIndex, home.R, home.Q)),
                    home => RwardPositive(new CoordinateKey(faceIndex.nextIndex, home.R, home.Q))
                };

                List<Directional> bottomRightEdgeDirectionals = new List<Directional>
                {
                    QwardPositive,
                    QwardNegative,
                    RwardAcross,
                    RwardPositive,
                    home => QwardAcross(new CoordinateKey(faceIndex.currIndex, home.R, home.Q)),
                    home => QwardPositive(new CoordinateKey(faceIndex.currIndex, home.R, home.Q))
                };

                _edgeNeighbors[faceIndex.currIndex].SetFloorEdge(bottomFloorEdgeDirectionals);
                _edgeNeighbors[bottomMiddleOffset].SetFloorEdge(middleFloorEdgeDirectionals);
                _edgeNeighbors[faceIndex.currIndex].SetLeftEdge(bottomLeftEdgeDirectionals);

                _edgeNeighbors[faceIndex.nextIndex].SetRightEdge(bottomRightEdgeDirectionals);


                bottomMiddleOffset += 2;
            }
        }

        private void AddCoordinate(int p, int q, int r)
        {
            _coordinates[p][q][r] = new CoordinateValue(p, q, r);
        }

        public bool IsValidCoordinate(CoordinateKey coord)
        {
            // Coordinates can't be negative or greater than the side length
            return coord.Q < _sideLength && coord.R < _sideLength
                   && coord.Q >= 0 && coord.R >= 0
                   && coord.Q + coord.R < _sideLength;
        }

        private int MaxSideLength()
        {
            return _sideLength - 1;
        }

    }

    internal struct FaceIndex
    {
        public int currIndex { get; }
        public int nextIndex { get; }
        public int afterNextIndex { get; }

        public FaceIndex(int currI, int nextI, int afterNextI)
        {
            currIndex = currI;
            nextIndex = nextI;
            afterNextIndex = afterNextI;
        }
    }
}