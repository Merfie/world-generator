﻿using System;

namespace GlobeGenerator.Globe
{
    public struct CoordinateKey
    {
        private readonly int p;
        private readonly int q;
        private readonly int r;

        public int P => p;
        public int Q => q;
        public int R => r;

        public CoordinateKey(int p, int q, int r)
        {
            this.p = p;
            this.q = q;
            this.r = r;
        }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + P;
            result = 31 * result + Q;
            result = 31 * result + R;
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            CoordinateKey coord = (CoordinateKey) obj;
            return P == coord.P && Q == coord.Q && R == coord.R;
        }

        public override string ToString()
        {
            return $"P: {P}, Q: {Q}, R: {R}";
        }
    }
}