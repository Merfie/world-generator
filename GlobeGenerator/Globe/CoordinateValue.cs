﻿using System;
using System.Collections.Generic;

namespace GlobeGenerator.Globe
{
    public class CoordinateValue
    {
        private readonly string _id;
        private Dictionary<string, object> attributes;

        public CoordinateValue(int p, int q, int r)
        {
            _id = MakeId(p, q, r);
            attributes = new Dictionary<string, object>();

            InitializeBasicAttributes();
        }

        private void InitializeBasicAttributes()
        {
            attributes.Add("height", null);
            attributes.Add("vector_x", null);
            attributes.Add("vector_y", null);
            attributes.Add("vector_z", null);
        }

        private string MakeId(int p, int q, int r)
        {
            return $"{p}{p}{p}{q}{q}{q}{r}{r}{r}";
        }

        public void SetRawHeight(int height)
        {
            attributes["height"] = height;
        }

        public int? GetRawHeight()
        {
            return Convert.ToInt32(attributes["height"]);
        }

        public void SetVector(double? x, double? y, double? z)
        {
            attributes["vector_x"] = x;
            attributes["vector_y"] = y;
            attributes["vector_z"] = z;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            CoordinateValue coord = (CoordinateValue) obj;
            return _id == coord._id;
        }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + _id.GetHashCode();
            return result;
        }

        public string GetId()
        {
            return _id;
        }

        public override string ToString()
        {
            return $"CoordinateValue {_id}";
        }

        public Vector GetVector()
        {
            return new Vector(GetDouble("vector_x"), GetDouble("vector_y"), GetDouble("vector_z"));
        }

        public double? GetDouble(string attribute)
        {
            object result = attributes[attribute];
            if (result != null)
            {
                return Convert.ToDouble(result);
            }

            return null;
        }
    }
}