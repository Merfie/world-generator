﻿using System.Collections.Generic;

namespace GlobeGenerator.Globe
{
    public class FaceEdges
    {
        private List<Directional> _rightEdge;
        private List<Directional> _leftEdge;
        private List<Directional> _floorEdge;

        public void SetRightEdge(List<Directional> edge)
        {
            _rightEdge = edge;
        }

        public void SetLeftEdge(List<Directional> edge)
        {
            _leftEdge = edge;
        }

        public void SetFloorEdge(List<Directional> edge)
        {
            _floorEdge = edge;
        }

        public List<Directional> GetEdge(CoordinateKey home)
        {
            if (home.Q == 0)
            {
                return _leftEdge;
            }

            if (home.R == 0)
            {
                return _rightEdge;
            }

            // bottom edge as default here, nothing defensive, assume we're not fed bad data
            return _floorEdge;

        }
    }
}