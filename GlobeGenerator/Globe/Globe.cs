﻿using System.Collections.Generic;
using System.Linq;

namespace GlobeGenerator.Globe
{
    public class Globe
    {
        private readonly List<Face> _coordinates;
        private readonly Dictionary<CoordinateKey, HashSet<CoordinateKey>> _cornerNeighbors;
        private readonly List<FaceEdges> _edgeNeighbors;
        private readonly int _maxSideLength;

        public Globe(List<Face> coordinates, Dictionary<CoordinateKey, HashSet<CoordinateKey>> cornerNeighbors, List<FaceEdges> edgeNeighbors, int maxSideLength)
        {
            _coordinates = coordinates;
            _cornerNeighbors = cornerNeighbors;
            _edgeNeighbors = edgeNeighbors;
            _maxSideLength = maxSideLength;
        }

        public CoordinateValue GetCoordinate(int p, int q, int r)
        {
            return _coordinates[p].GetCoordinate(q, r);
        }

        public HashSet<CoordinateValue> GetNeighbors(CoordinateKey home)
        {
            HashSet<CoordinateValue> results = new HashSet<CoordinateValue>();

            if (IsCenter(home))
            {
                foreach (Directional direction in GlobeBuilder.Directions)
                {
                    results.Add(GetCoordinate(direction(home)));
                }
            }
            else if (IsCorner(home))
            {
                foreach (CoordinateKey neighborKey in _cornerNeighbors[home])
                {
                    results.Add(GetCoordinate(neighborKey));
                }
            }
            else {
                foreach (Directional direction in _edgeNeighbors[home.P].GetEdge(home))
                {
                    results.Add(GetCoordinate(direction(home)));
                }

            }
            return results;
        }

        public CoordinateValue GetCoordinate(CoordinateKey home)
        {
            return GetCoordinate(home.P, home.Q, home.R);
        }

        public IEnumerable<Face> GetFaces()
        {
            return Enumerable.Range(0, 20).Select(faceIndex => _coordinates[faceIndex]);
        }

        private bool IsCenter(CoordinateKey home)
        {
            return home.Q != 0 && home.R != 0 && home.Q + home.R < _maxSideLength;
        }

        private bool IsCorner(CoordinateKey home)
        {
            return home.Q == 0 && home.R == 0 ||
                   home.Q == 0 && home.R == _maxSideLength ||
                   home.Q == _maxSideLength && home.R == 0;
        }
    }
}