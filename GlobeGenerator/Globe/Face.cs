﻿using System.Collections.Generic;

namespace GlobeGenerator.Globe
{
    public class Face
    {
        private readonly CoordinateValue[][] _coordinates;
        private readonly int _sideLength;
        private List<Triangle> _triangles;

        public Face(CoordinateValue[][] faceCoordinates, int sideLength, List<Triangle> triangles)
        {
            _coordinates = faceCoordinates;
            _sideLength = sideLength;
            _triangles = triangles;
        }

        public CoordinateValue GetCoordinate(int p, int q)
        {
            return _coordinates[p][q];
        }

        public IEnumerable<CoordinateValue> GetCoordinates()
        {
            for (int q = 0; q <= _sideLength; q++)
            {
                for (int r = 0; r < _sideLength - q; r++)
                {
                    yield return GetCoordinate(q, r);
                }
            }
        }

        public List<Triangle> GetTriangles()
        {
            return _triangles;
        }
    }
}