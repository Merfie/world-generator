﻿namespace GlobeGenerator.Globe
{
    public struct Vector
    {
        public double? X { get; }
        public double? Y { get; }
        public double? Z { get; }

        public Vector(double? x, double? y, double? z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public bool IsNull()
        {
            return X == null && Y == null && Z == null;
        }

        /*
            This method will return a null-safe version of X
            Tradeoff: It will not throw an exception or fail, it will simply return 0.0 in case of null
            Do not call this method unless you have first called IsNull() if you want to see the result of the vector
         */
        public double XNullSafe()
        {
            if (X != null)
            {
                return (double) X;
            }

            return 0.0;
        }

        /*
            This method will return a null-safe version of Y
            Tradeoff: It will not throw an exception or fail, it will simply return 0.0 in case of null
            Do not call this method unless you have first called IsNull() if you want to see the result of the vector
         */
        public double YNullSafe()
        {
            if (Y != null)
            {
                return (double) Y;
            }

            return 0.0;
        }

        /*
            This method will return a null-safe version of Y
            Tradeoff: It will not throw an exception or fail, it will simply return 0.0 in case of null
            Do not call this method unless you have first called IsNull() if you want to see the result of the vector
         */
        public double ZNullSafe()
        {
            if (Z != null)
            {
                return (double) Z;
            }

            return 0.0;
        }
    }
}