﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{
    public partial class App : Form
    {
        public App()
        {
            Random rng = new Random();
            int seed = rng.Next(0, int.MaxValue);
            HeightMapper mapper = new HeightMapper(seed);
            InitializeComponent();
        }
    }
}
